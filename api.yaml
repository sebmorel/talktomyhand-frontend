openapi: '3.0.3'
info:
  title: Talk To My Hand API
  version: '0.0.8'
  description: This API is used to communicate with the TTMY Backend Service.
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
externalDocs:
  description: Git Repository
  url: 'https://gitlab.com/CosmicDarine/talk-to-my-hand'
servers:
  - url: 'http://localhost:8081'

tags:
  - name: Authentication
    description: "All endpoints used for authentication and registration."
  - name: Category
    description: "All endpoints used to manipulate categories."
  - name: User
    description: "All endpoints used for user modification."
  - name: Dialect
    description: "All endpoints used to add, modify and remove elements linked to the dialect part of the app."
  - name: Image
    description: "All endpoints used to add, modify and remove images in the app."
  - name: Lesson
    description: "All endpoints used to add, modify and remove lessons in the app."

# Here the security is applied globally.
security:
  - JWTSecurity: []

#########
# PATHS #
#########
paths:
  # AUTHENTICATION
  /auth/register:
    post:
      tags:
        - Authentication
      security: [ ]   # No security
      description: This public endpoint is used to register a new user.
      summary: Register a new user.
      operationId: register
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RegisterDTO'
      responses:
        '201':
          description: Register successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'
        '400':
          description: Username or Email Already taken.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'
  /auth/login:
    post:
      tags:
        - Authentication
      security: [ ]   # No security
      description: This public endpoint is used to login an existing user.
      summary: Login
      operationId: login
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginRequestDTO'
      responses:
        '200':
          description: Login successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginSuccessDTO'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'
  /auth/logout:
    post:
      tags:
        - Authentication
      description: This private endpoint is used to logout a logged user.
      summary: Logout.
      operationId: logout
      responses:
        '200':
          description: Logout successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'
        '401':
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'

  # IMAGE
  /images/user/{username}:
    get:
      tags:
        - Image
      description: This private endpoint is used to get a users profile picture.
      summary: Get a specific users profile picture.
      operationId: getImageUser
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get users profile picture successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
  /images/user:
    get:
      tags:
        - Image
      description: This private endpoint is used to get the current users profile picture.
      summary: Change a users profile picture.
      operationId: getCurrentUserImage
      responses:
        '200':
          description: Got the current users picture successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
    post:
      tags:
        - Image
      description: This private endpoint is used to create a users profile picture.
      summary: Change a users profile picture.
      operationId: createImageUser
      requestBody:
        content:
          application/octet-stream:
            schema:
              type: string
              format: base64
      responses:
        '200':
          description: Created users picture successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
    put:
      tags:
        - Image
      description: This private endpoint is used to change a users profile picture.
      summary: Change a users profile picture.
      operationId: updateImageUser
      requestBody:
        content:
          application/octet-stream:
            schema:
              type: string
              format: base64
      responses:
        '200':
          description: Update users picture successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
    delete:
      tags:
        - Image
      description: This private endpoint is used to delete a users profile picture.
      summary: Change a users profile picture.
      operationId: deleteImageUser
      responses:
        '200':
          description: Update users picture successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserSimpleDTO'
  /images/sign/{name}:
    post:
      tags:
        - Image
      description: This private endpoint is used to create a sign picture.
      summary: Create a signs picture.
      operationId: createImageSign
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      requestBody:
        content:
          application/octet-stream:
            schema:
              type: string
              format: base64
      responses:
        '200':
          description: Sign image creation successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
    put:
      tags:
        - Image
      description: This private endpoint is used to change a sign picture.
      summary: Change a signs picture.
      operationId: updateImageSign
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      requestBody:
        content:
          application/octet-stream:
            schema:
              type: string
              format: base64
      responses:
        '200':
          description: Sign image update successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ImageDTO'
    delete:
      tags:
        - Image
      description: This private endpoint is used to delete a sign picture.
      summary: Delete a sign picture.
      operationId: deleteImageSign
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Sign image removal successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SignDTO'
  # USER
  /admin/users:
    get:
      tags:
        - User
      description: This private endpoint is used to get all users.
      summary: Get all users.
      operationId: getUsers
      responses:
        '200':
          description: Get users successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/UserSimpleDTO'
    delete:
      tags:
        - User
      description: This private endpoint is used to remove all users.
      summary: Delete all users.
      operationId: deleteUsers
      responses:
        '200':
          description: Users deletion successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'
  /admin/users/{username}/points:
    get:
      tags:
        - User
      description: This private endpoint is used to get the points of a specific user.
      summary: Get the points of a specific user.
      operationId: getPoints
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get user's points successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/UserDTO'
    put:
      tags:
        - User
      description: This private endpoint is used to modify points of one user.
      summary: Update a points of a specific user.
      operationId: updateUserPoints
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/UserModificationDTO'
      responses:
        '200':
          description: User update successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /users/{username}:
    get:
      tags:
        - User
      description: This private endpoint is used to get one user.
      summary: Get a specific user.
      operationId: getUser
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get user successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
    put:
      tags:
        - User
      description: This private endpoint is used to modify one user.
      summary: Update a specific user.
      operationId: updateUser
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/UserModificationDTO'
      responses:
        '200':
          description: User update successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
    delete: #TODO : check if secure
      tags:
        - User
      description: This private endpoint is used to remove one user.
      summary: Delete a specific user.
      operationId: deleteUser
      parameters:
        - in: path
          name: username
          schema:
            type: string
          required: true
      responses:
        '200':
          description: User deletion successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ApiMessageDTO'

  # DIALECT
  # Sign
  /signs/search/{keyword}:
    get:
      tags:
        - Dialect
      description: This private endpoint is used to get all the sign that are linked to the keyword in the data base.
      summary: Search sign.
      operationId: searchSign
      parameters:
        - in: path
          name: keyword
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Search sign successfully.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/SignDTO'
  /signs/{name}:
    get:
      tags:
        - Dialect
      description: This private endpoint is used to get the sign with the given name in the database.
      summary: Get sign.
      operationId: getSignByName
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Got sign successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SignDTO'
  /signs:
    get:
      tags:
        - Dialect
      description: This private endpoint is used to get all the sign in the database.
      summary: Get all sign.
      operationId: getSign
      responses:
        '200':
          description: Get sign successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/SignDTO'
    post:
      tags:
        - Dialect
      description: This private endpoint is used to add a new sign in the database.
      summary: Add the sign.
      operationId: createSign
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/SignCreationDTO'
      responses:
        '200':
          description: Sign creation successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SignDTO'
    put:
      tags:
        - Dialect
      description: This private endpoint is used to modifiy a sign in the database.
      summary: Modifiy the sign.
      operationId: updateSign
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/SignModificationDTO'
      responses:
        '200':
          description: Sign modification successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SignDTO'
  /lessons/add-sign/{name}:
    put:
      tags:
        - Lesson
      description: This private endpoint is used to add a sign to the lesson with the given lesson's name in the database.
      summary: Add sign to lesson.
      operationId: addSignLesson
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/SignInCreationDTO'
      responses:
        '200':
          description: Added Sign successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LessonDTO'
  /lessons/subscribe/{name}:
    put:
      tags:
        - Lesson
      description: This private endpoint is used to subscribe the current user to the lesson with the given id in the database.
      summary: Subscribe to lesson.
      operationId: subscribeToLesson
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Subscribed to lesson successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LessonDTO'
  /lessons/{name}:
    get:
      tags:
        - Lesson
      description: This private endpoint is used to get the lesson with the given id in the database.
      summary: Get lesson.
      operationId: getLesson
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get lesson successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LessonDTO'
    put:
      tags:
        - Lesson
      description: This private endpoint is used to update the lesson with the given id in the database.
      summary: Update lesson.
      operationId: updateLesson
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/LessonDTO'
      responses:
        '200':
          description: Updated lesson successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LessonDTO'
  /lessons/category/{category}:
    get:
      tags:
        - Lesson
      description: This private endpoint is used to get all the lessons of a user and a given category in the database.
      summary: Get all lessons of given category.
      operationId: getLessonsOfCategory
      parameters:
        - in: path
          name: category
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get lessons of given category successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/LessonDTO'
  /lessons:
    get:
      tags:
        - Lesson
      description: This private endpoint is used to get all the lessons of a user in the database.
      summary: Get all lessons.
      operationId: getLessons
      responses:
        '200':
          description: Get lessons successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/LessonDTO'
    post:
      tags:
        - Lesson
      description: This private endpoint is used to add a new lesson in the database.
      summary: Add the lesson.
      operationId: createLesson
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/LessonCreationDTO'
      responses:
        '200':
          description: Lesson creation successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LessonDTO'
  # Category
  /categories/{name}:
    get:
      tags:
        - Category
      description: This private endpoint is used to get the lesson category with the given id in the database.
      summary: Get lesson category.
      operationId: getCategory
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Get lesson category successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CategoryDTO'
    put:
      tags:
        - Category
      description: This private endpoint is used to update a lesson category in the database.
      summary: Add the lesson category.
      operationId: updateCategory
      parameters:
        - in: path
          name: name
          schema:
            type: string
          required: true
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/CategoryModificationDTO'
      responses:
        '200':
          description: Lesson category updated successfully.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CategoryDTO'
  /categories:
    get:
      tags:
        - Category
      description: This private endpoint is used to get all the sign categories of a user in the database.
      summary: Get all sign categories.
      operationId: getCategories
      responses:
        '200':
          description: Get sign categories successful.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/CategoryDTO'
    post:
      tags:
        - Category
      description: This private endpoint is used to add a new sign category in the database.
      summary: Add the sign category.
      operationId: createCategory
      requestBody:
        content:
          'application/json':
            schema:
              $ref: '#/components/schemas/CategoryCreationDTO'
      responses:
        '200':
          description: sign category creation successful.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CategoryDTO'

##############
# COMPONENTS #
##############
components:
  securitySchemes:
    JWTSecurity:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    # API MESSAGE
    ApiMessageDTO:
      title: ApiMessageDTO
      type: object
      properties:
        code:
          type: integer
        type:
          type: string
        message:
          type: string

    # REGISTER
    RegisterDTO:
      title: RegisterDTO
      type: object
      properties:
        username:
          type: string
          example: 'cuicui'
        email:
          type: string
          example: 'cuicui@email.com'
        password:
          type: string
          example: 'cui'
        firstname:
          type: string
          example: 'Olivier'
        lastname:
          type: string
          example: 'Cuisenaire'
        sex:
          type: string
          example: 'MALE'
        birthdate:
          type: string
          format: date
          example: '1972-11-24'
    # LOGIN REQUEST
    LoginRequestDTO:
      title: LoginRequestDTO
      type: object
      properties:
        usernameOrEmail:
          type: string
          example: 'cuicui'
        password:
          type: string
          example: 'cui'
    # LOGIN SUCCESS
    LoginSuccessDTO:
      title: LoginSuccessDTO
      type: object
      properties:
        jwt:
          type: string
        username:
          type: string
        roles:
          type: array
          items:
            type: string

    # ROLE
    RoleDTO:
      title: RoleDTO
      type: object
      properties:
        id:
          type: integer
          example: 1
        name:
          type: string
          nullable: true
          example: 'USER'

    # USER
    UserDTO:
      title: UserDTO
      type: object
      properties:
        idUser:
          type: integer
        username:
          type: string
        firstname:
          type: string
        lastname:
          type: string
        birthdate:
          type: string
          format: date
        email:
          type: string
        password:
          type: string
        sex:
          type: string
        country:
          type: string
        points:
          type: integer
        roles:
          type: array
          items:
            $ref: '#/components/schemas/RoleDTO'
        image:
          $ref: '#/components/schemas/ImageDTO'
    UserModificationDTO:
      title: UserModificationDTO
      type: object
      properties:
        idUser:
          type: integer
        username:
          type: string
        firstname:
          type: string
        lastname:
          type: string
        birthdate:
          type: string
          format: date
        sex:
          type: string
        email:
          type: string
        country:
          type: string
        points:
          type: integer
        password:
          type: string
    UserSimpleDTO:
      title: UserModificationDTO
      type: object
      properties:
        username:
          type: string
        firstname:
          type: string
        lastname:
          type: string
        birthdate:
          type: string
          format: date
        sex:
          type: string
        country:
          type: string
        points:
          type: integer
        email:
          type: string

    # DIALECT
    # SIGN IN
    SignInCreationDTO:
      title: SignInCreationDTO
      type: object
      properties:
        sign:
          type: string
          nullable: false
        progression:
          type: integer
        date:
          type: string
          format: date
          example: '2021-03-03'
    SignInDTO:
      title: SignInDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        progression:
          type: integer
        date:
          type: string
          format: date
          example: '2021-03-03'
        sign:
          $ref: '#/components/schemas/SignDTO'
    # SIGN
    SignCreationDTO:
      title: SignCreationDTO
      type: object
      properties:
        traduction:
          type: string
          nullable: false
        description:
          type: string
          nullable: false
        video:
          type: string
          nullable: false
        difficulty:
          type: string
          nullable: false
    SignModificationDTO:
      title: SignModificationDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        traduction:
          type: string
          nullable: false
        description:
          type: string
          nullable: false
        video:
          type: string
          nullable: false
        difficulty:
          type: string
          nullable: false
    SignDTO:
      title: SignDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        traduction:
          type: string
          nullable: false
        description:
          type: string
          nullable: false
        video:
          type: string
          nullable: false
        difficulty:
          type: string
          nullable: false
    #LESSON
    LessonCreationDTO:
      title: LessonCreationDTO
      type: object
      properties:
        name:
          type: string
          nullable: false
        difficulty:
          type: integer
        reward:
          type: integer
        signs:
          type: array
          items:
            $ref: '#/components/schemas/SignInCreationDTO'
    LessonDTO:
      title: LessonDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        name:
          type: string
          nullable: false
        difficulty:
          type: integer
        reward:
          type: integer
        subscribers:
          type: array
          items:
            $ref: '#/components/schemas/UserSimpleDTO'
        signs:
          type: array
          items:
            $ref: '#/components/schemas/SignInDTO'
    # CATEGORY
    CategoryCreationDTO:
      title: CategoryCreationDTO
      type: object
      properties:
        name:
          type: string
          nullable: false
    CategoryModificationDTO:
      title: CategoryModificationDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        name:
          type: string
          nullable: false
    CategoryDTO:
      title: CategoryDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        name:
          type: string
          nullable: false
        users:
          type: array
          items:
            $ref: '#/components/schemas/UserSimpleDTO'
    # IMAGE
    ImageCreationDTO:
      title: ImageCreationDTO
      type: object
      properties:
        image:
          type: string
          format: base64
          nullable: false
    ImageDTO:
      title: ImageDTO
      type: object
      properties:
        id:
          type: integer
          nullable: false
        image:
          type: string
          format: base64
          nullable: false

