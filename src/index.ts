/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
export { ApiError } from './core/ApiError';
export { OpenAPI } from './core/OpenAPI';

export type { ApiMessageDTO } from './models/ApiMessageDTO';
export type { CategoryCreationDTO } from './models/CategoryCreationDTO';
export type { CategoryDTO } from './models/CategoryDTO';
export type { CategoryModificationDTO } from './models/CategoryModificationDTO';
export type { ImageCreationDTO } from './models/ImageCreationDTO';
export type { ImageDTO } from './models/ImageDTO';
export type { LessonCreationDTO } from './models/LessonCreationDTO';
export type { LessonDTO } from './models/LessonDTO';
export type { LoginRequestDTO } from './models/LoginRequestDTO';
export type { LoginSuccessDTO } from './models/LoginSuccessDTO';
export type { RegisterDTO } from './models/RegisterDTO';
export type { RoleDTO } from './models/RoleDTO';
export type { SignCreationDTO } from './models/SignCreationDTO';
export type { SignDTO } from './models/SignDTO';
export type { SignInCreationDTO } from './models/SignInCreationDTO';
export type { SignInDTO } from './models/SignInDTO';
export type { SignModificationDTO } from './models/SignModificationDTO';
export type { UserDTO } from './models/UserDTO';
export type { UserModificationDTO } from './models/UserModificationDTO';
export type { UserSimpleDTO } from './models/UserSimpleDTO';

export { AuthenticationService } from './services/AuthenticationService';
export { CategoryService } from './services/CategoryService';
export { DialectService } from './services/DialectService';
export { ImageService } from './services/ImageService';
export { LessonService } from './services/LessonService';
export { UserService } from './services/UserService';
