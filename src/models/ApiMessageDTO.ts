/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ApiMessageDTO = {
    code?: number;
    type?: string;
    message?: string;
}