/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CategoryCreationDTO = {
    name?: string;
}