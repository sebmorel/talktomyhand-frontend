/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { UserSimpleDTO } from './UserSimpleDTO';

export type CategoryDTO = {
    id?: number;
    name?: string;
    users?: Array<UserSimpleDTO>;
}