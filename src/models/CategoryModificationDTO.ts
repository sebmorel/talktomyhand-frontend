/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CategoryModificationDTO = {
    id?: number;
    name?: string;
}