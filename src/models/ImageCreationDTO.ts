/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ImageCreationDTO = {
    image?: string;
}