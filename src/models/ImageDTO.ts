/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type ImageDTO = {
    id?: number;
    image?: string;
}