/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SignInCreationDTO } from './SignInCreationDTO';

export type LessonCreationDTO = {
    name?: string;
    difficulty?: number;
    reward?: number;
    signs?: Array<SignInCreationDTO>;
}