/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SignInDTO } from './SignInDTO';
import type { UserSimpleDTO } from './UserSimpleDTO';

export type LessonDTO = {
    id?: number;
    name?: string;
    difficulty?: number;
    reward?: number;
    subscribers?: Array<UserSimpleDTO>;
    signs?: Array<SignInDTO>;
}