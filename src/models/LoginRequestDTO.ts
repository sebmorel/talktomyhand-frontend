/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LoginRequestDTO = {
    usernameOrEmail?: string;
    password?: string;
}