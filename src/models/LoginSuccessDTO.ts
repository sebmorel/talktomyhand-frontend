/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type LoginSuccessDTO = {
    jwt?: string;
    username?: string;
    roles?: Array<string>;
}