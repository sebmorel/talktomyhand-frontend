/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type RegisterDTO = {
    username?: string;
    email?: string;
    password?: string;
    firstname?: string;
    lastname?: string;
    sex?: string;
    birthdate?: string;
}