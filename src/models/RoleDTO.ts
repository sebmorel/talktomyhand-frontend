/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type RoleDTO = {
    id?: number;
    name?: string | null;
}