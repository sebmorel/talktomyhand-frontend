/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignCreationDTO = {
    traduction?: string;
    description?: string;
    video?: string;
    difficulty?: string;
}