/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignInCreationDTO = {
    sign?: string;
    progression?: number;
    date?: string;
}