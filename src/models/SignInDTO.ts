/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { SignDTO } from './SignDTO';

export type SignInDTO = {
    id?: number;
    progression?: number;
    date?: string;
    sign?: SignDTO;
}