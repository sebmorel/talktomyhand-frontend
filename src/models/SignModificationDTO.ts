/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type SignModificationDTO = {
    id?: number;
    traduction?: string;
    description?: string;
    video?: string;
    difficulty?: string;
}