/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ImageDTO } from './ImageDTO';
import type { RoleDTO } from './RoleDTO';

export type UserDTO = {
    idUser?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    email?: string;
    password?: string;
    sex?: string;
    country?: string;
    points?: number;
    roles?: Array<RoleDTO>;
    image?: ImageDTO;
}