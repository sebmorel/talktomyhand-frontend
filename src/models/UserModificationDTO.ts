/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserModificationDTO = {
    idUser?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    sex?: string;
    email?: string;
    country?: string;
    points?: number;
    password?: string;
}