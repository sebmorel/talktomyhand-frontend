/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type UserSimpleDTO = {
    username?: string;
    firstname?: string;
    lastname?: string;
    birthdate?: string;
    sex?: string;
    country?: string;
    points?: number;
    email?: string;
}