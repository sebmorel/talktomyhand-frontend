import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from "../components/pages/Home.vue";
import Register from "../components/pages/Register.vue";
import Categories from "../components/pages/Categories.vue";
import Login from "../components/pages/Login.vue";
import Sign from "../components/uniqueComponents/Sign.vue";

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/sign',
    name: 'Sign',
    component: Sign
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/categories",
    name: "categories",
    component: Categories
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

export default router
