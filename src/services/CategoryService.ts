/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CategoryCreationDTO } from '../models/CategoryCreationDTO';
import type { CategoryDTO } from '../models/CategoryDTO';
import type { CategoryModificationDTO } from '../models/CategoryModificationDTO';
import { request as __request } from '../core/request';

export class CategoryService {

    /**
     * Get lesson category.
     * This private endpoint is used to get the lesson category with the given id in the database.
     * @param name 
     * @returns CategoryDTO Get lesson category successful.
     * @throws ApiError
     */
    public static async getCategory(
name: string,
): Promise<CategoryDTO> {
        const result = await __request({
            method: 'GET',
            path: `/categories/${name}`,
        });
        return result.body;
    }

    /**
     * Add the lesson category.
     * This private endpoint is used to update a lesson category in the database.
     * @param name 
     * @param requestBody 
     * @returns CategoryDTO Lesson category updated successfully.
     * @throws ApiError
     */
    public static async updateCategory(
name: string,
requestBody?: CategoryModificationDTO,
): Promise<CategoryDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/categories/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get all sign categories.
     * This private endpoint is used to get all the sign categories of a user in the database.
     * @returns CategoryDTO Get sign categories successful.
     * @throws ApiError
     */
    public static async getCategories(): Promise<Array<CategoryDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/categories`,
        });
        return result.body;
    }

    /**
     * Add the sign category.
     * This private endpoint is used to add a new sign category in the database.
     * @param requestBody 
     * @returns CategoryDTO sign category creation successful.
     * @throws ApiError
     */
    public static async createCategory(
requestBody?: CategoryCreationDTO,
): Promise<CategoryDTO> {
        const result = await __request({
            method: 'POST',
            path: `/categories`,
            body: requestBody,
        });
        return result.body;
    }

}