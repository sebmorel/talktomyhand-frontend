/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { SignCreationDTO } from '../models/SignCreationDTO';
import type { SignDTO } from '../models/SignDTO';
import type { SignModificationDTO } from '../models/SignModificationDTO';
import { request as __request } from '../core/request';

export class DialectService {

    /**
     * Search sign.
     * This private endpoint is used to get all the sign that are linked to the keyword in the data base.
     * @param keyword 
     * @returns SignDTO Search sign successfully.
     * @throws ApiError
     */
    public static async searchSign(
keyword: string,
): Promise<Array<SignDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/signs/search/${keyword}`,
        });
        return result.body;
    }

    /**
     * Get sign.
     * This private endpoint is used to get the sign with the given name in the database.
     * @param name 
     * @returns SignDTO Got sign successfully.
     * @throws ApiError
     */
    public static async getSignByName(
name: string,
): Promise<SignDTO> {
        const result = await __request({
            method: 'GET',
            path: `/signs/${name}`,
        });
        return result.body;
    }

    /**
     * Get all sign.
     * This private endpoint is used to get all the sign in the database.
     * @returns SignDTO Get sign successful.
     * @throws ApiError
     */
    public static async getSign(): Promise<Array<SignDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/signs`,
        });
        return result.body;
    }

    /**
     * Add the sign.
     * This private endpoint is used to add a new sign in the database.
     * @param requestBody 
     * @returns SignDTO Sign creation successful.
     * @throws ApiError
     */
    public static async createSign(
requestBody?: SignCreationDTO,
): Promise<SignDTO> {
        const result = await __request({
            method: 'POST',
            path: `/signs`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Modifiy the sign.
     * This private endpoint is used to modifiy a sign in the database.
     * @param requestBody 
     * @returns SignDTO Sign modification successful.
     * @throws ApiError
     */
    public static async updateSign(
requestBody?: SignModificationDTO,
): Promise<SignDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/signs`,
            body: requestBody,
        });
        return result.body;
    }

}