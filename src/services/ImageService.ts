/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ImageDTO } from '../models/ImageDTO';
import type { SignDTO } from '../models/SignDTO';
import type { UserSimpleDTO } from '../models/UserSimpleDTO';
import { request as __request } from '../core/request';

export class ImageService {

    /**
     * Get a specific users profile picture.
     * This private endpoint is used to get a users profile picture.
     * @param username 
     * @returns ImageDTO Get users profile picture successful.
     * @throws ApiError
     */
    public static async getImageUser(
username: string,
): Promise<ImageDTO> {
        const result = await __request({
            method: 'GET',
            path: `/images/user/${username}`,
        });
        return result.body;
    }

    /**
     * Change a users profile picture.
     * This private endpoint is used to get the current users profile picture.
     * @returns ImageDTO Got the current users picture successfully.
     * @throws ApiError
     */
    public static async getCurrentUserImage(): Promise<ImageDTO> {
        const result = await __request({
            method: 'GET',
            path: `/images/user`,
        });
        return result.body;
    }

    /**
     * Change a users profile picture.
     * This private endpoint is used to create a users profile picture.
     * @param requestBody 
     * @returns ImageDTO Created users picture successfully.
     * @throws ApiError
     */
    public static async createImageUser(
requestBody?: any,
): Promise<ImageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/images/user`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Change a users profile picture.
     * This private endpoint is used to change a users profile picture.
     * @param requestBody 
     * @returns ImageDTO Update users picture successful.
     * @throws ApiError
     */
    public static async updateImageUser(
requestBody?: any,
): Promise<ImageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/images/user`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Change a users profile picture.
     * This private endpoint is used to delete a users profile picture.
     * @returns UserSimpleDTO Update users picture successful.
     * @throws ApiError
     */
    public static async deleteImageUser(): Promise<UserSimpleDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/images/user`,
        });
        return result.body;
    }

    /**
     * Create a signs picture.
     * This private endpoint is used to create a sign picture.
     * @param name 
     * @param requestBody 
     * @returns ImageDTO Sign image creation successful.
     * @throws ApiError
     */
    public static async createImageSign(
name: string,
requestBody?: any,
): Promise<ImageDTO> {
        const result = await __request({
            method: 'POST',
            path: `/images/sign/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Change a signs picture.
     * This private endpoint is used to change a sign picture.
     * @param name 
     * @param requestBody 
     * @returns ImageDTO Sign image update successful.
     * @throws ApiError
     */
    public static async updateImageSign(
name: string,
requestBody?: any,
): Promise<ImageDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/images/sign/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a sign picture.
     * This private endpoint is used to delete a sign picture.
     * @param name 
     * @returns SignDTO Sign image removal successful.
     * @throws ApiError
     */
    public static async deleteImageSign(
name: string,
): Promise<SignDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/images/sign/${name}`,
        });
        return result.body;
    }

}