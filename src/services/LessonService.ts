/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { LessonCreationDTO } from '../models/LessonCreationDTO';
import type { LessonDTO } from '../models/LessonDTO';
import type { SignInCreationDTO } from '../models/SignInCreationDTO';
import { request as __request } from '../core/request';

export class LessonService {

    /**
     * Add sign to lesson.
     * This private endpoint is used to add a sign to the lesson with the given lesson's name in the database.
     * @param name 
     * @param requestBody 
     * @returns LessonDTO Added Sign successfully.
     * @throws ApiError
     */
    public static async addSignLesson(
name: string,
requestBody?: SignInCreationDTO,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/add-sign/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Subscribe to lesson.
     * This private endpoint is used to subscribe the current user to the lesson with the given id in the database.
     * @param name 
     * @returns LessonDTO Subscribed to lesson successfully.
     * @throws ApiError
     */
    public static async subscribeToLesson(
name: string,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/subscribe/${name}`,
        });
        return result.body;
    }

    /**
     * Get lesson.
     * This private endpoint is used to get the lesson with the given id in the database.
     * @param name 
     * @returns LessonDTO Get lesson successful.
     * @throws ApiError
     */
    public static async getLesson(
name: string,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'GET',
            path: `/lessons/${name}`,
        });
        return result.body;
    }

    /**
     * Update lesson.
     * This private endpoint is used to update the lesson with the given id in the database.
     * @param name 
     * @param requestBody 
     * @returns LessonDTO Updated lesson successfully.
     * @throws ApiError
     */
    public static async updateLesson(
name: string,
requestBody?: LessonDTO,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/lessons/${name}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get all lessons of given category.
     * This private endpoint is used to get all the lessons of a user and a given category in the database.
     * @param category 
     * @returns LessonDTO Get lessons of given category successful.
     * @throws ApiError
     */
    public static async getLessonsOfCategory(
category: string,
): Promise<Array<LessonDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/lessons/category/${category}`,
        });
        return result.body;
    }

    /**
     * Get all lessons.
     * This private endpoint is used to get all the lessons of a user in the database.
     * @returns LessonDTO Get lessons successful.
     * @throws ApiError
     */
    public static async getLessons(): Promise<Array<LessonDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/lessons`,
        });
        return result.body;
    }

    /**
     * Add the lesson.
     * This private endpoint is used to add a new lesson in the database.
     * @param requestBody 
     * @returns LessonDTO Lesson creation successful.
     * @throws ApiError
     */
    public static async createLesson(
requestBody?: LessonCreationDTO,
): Promise<LessonDTO> {
        const result = await __request({
            method: 'POST',
            path: `/lessons`,
            body: requestBody,
        });
        return result.body;
    }

}