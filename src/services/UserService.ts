/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { ApiMessageDTO } from '../models/ApiMessageDTO';
import type { UserDTO } from '../models/UserDTO';
import type { UserModificationDTO } from '../models/UserModificationDTO';
import type { UserSimpleDTO } from '../models/UserSimpleDTO';
import { request as __request } from '../core/request';

export class UserService {

    /**
     * Get all users.
     * This private endpoint is used to get all users.
     * @returns UserSimpleDTO Get users successful.
     * @throws ApiError
     */
    public static async getUsers(): Promise<Array<UserSimpleDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/admin/users`,
        });
        return result.body;
    }

    /**
     * Delete all users.
     * This private endpoint is used to remove all users.
     * @returns ApiMessageDTO Users deletion successful.
     * @throws ApiError
     */
    public static async deleteUsers(): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/admin/users`,
        });
        return result.body;
    }

    /**
     * Get the points of a specific user.
     * This private endpoint is used to get the points of a specific user.
     * @param username 
     * @returns UserDTO Get user's points successful.
     * @throws ApiError
     */
    public static async getPoints(
username: string,
): Promise<Array<UserDTO>> {
        const result = await __request({
            method: 'GET',
            path: `/admin/users/${username}/points`,
        });
        return result.body;
    }

    /**
     * Update a points of a specific user.
     * This private endpoint is used to modify points of one user.
     * @param username 
     * @param requestBody 
     * @returns UserDTO User update successful.
     * @throws ApiError
     */
    public static async updateUserPoints(
username: string,
requestBody?: UserModificationDTO,
): Promise<UserDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/admin/users/${username}/points`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Get a specific user.
     * This private endpoint is used to get one user.
     * @param username 
     * @returns UserDTO Get user successful.
     * @throws ApiError
     */
    public static async getUser(
username: string,
): Promise<UserDTO> {
        const result = await __request({
            method: 'GET',
            path: `/users/${username}`,
        });
        return result.body;
    }

    /**
     * Update a specific user.
     * This private endpoint is used to modify one user.
     * @param username 
     * @param requestBody 
     * @returns UserDTO User update successful.
     * @throws ApiError
     */
    public static async updateUser(
username: string,
requestBody?: UserModificationDTO,
): Promise<UserDTO> {
        const result = await __request({
            method: 'PUT',
            path: `/users/${username}`,
            body: requestBody,
        });
        return result.body;
    }

    /**
     * Delete a specific user.
     * This private endpoint is used to remove one user.
     * @param username 
     * @returns ApiMessageDTO User deletion successful.
     * @throws ApiError
     */
    public static async deleteUser(
username: string,
): Promise<ApiMessageDTO> {
        const result = await __request({
            method: 'DELETE',
            path: `/users/${username}`,
        });
        return result.body;
    }

}