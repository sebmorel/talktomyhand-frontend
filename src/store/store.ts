import Vue from 'vue'
import Vuex from 'vuex'
//import connexion from './modules/connexion'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    //connexion
  }
})
